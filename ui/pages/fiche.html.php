<div class="row ">
    <h1 class="head-fiche text-center"><?php echo $pkm->get_nom(); ?></h1>  
</div>
<div class="row">
    <nav class="poke-nav">
        <?php
            if ($pkm->get_id() > 1) {
                $p2 = Pokemon_Db::get_by_id($pkm->get_id() -1);
                echo '<a href="index.php?a=fiche&id=' . $p2->get_id() . '" >'.$p2->get_id() . " " . $p2->get_nom().'</a>';
            }
            if ($pkm->get_id() < 649) {
                $p3 = Pokemon_Db::get_by_id($pkm->get_id() +1);
                echo '<a href="index.php?a=fiche&id=' . $p3->get_id() . '" class="pull-right">'.$p3->get_id() . " " . $p3->get_nom().'</a>';         
            }
        ?>
        <div class="clearfix"></div>
    </nav>
</div>

<div class="row">
    <div class="fiche-img col-lg-3">
        <img src=<?php echo Pokemon_Ui::get_image($pkm->get_id()) ?> alt="<?php $pkm->get_nom(); ?>">
    </div>

    <div class="col-lg-4">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <td>N° Nationnal</td><td><?php echo $pkm->get_id(); ?></td>
            </tr><tr>
                <td>Nom US</td><td><?php echo $pkm->get_nom_us(); ?></td>
            </tr><tr>
                <td>Nom Japonais</td><td><?php echo $pkm->get_nom_jp(); ?></td>
            </tr><tr>
                <td>Type</td><td>
                    <?php
                    echo Pokemon_Ui::type_button($pkm->get_type1());
                    if ($pkm->get_type2() != "") {
                        echo Pokemon_Ui::type_button($pkm->get_type2());
                    } ?>
                </td>
            </tr><tr>
                <td>Taille</td><td><?php echo $pkm->get_taille(); ?> m</td>
            </tr><tr>
                <td>Poids</td><td><?php echo $pkm->get_poids(); ?> kg</td>
            </tr><tr>
                <td>Espèce</td><td><?php echo $pkm->get_espece(); ?></td>
            </tr>

        </table>    
    </div>

    <div class="col-lg-4">
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <td>EV</td><td><?php echo $pkm->get_ev(); ?></td>
            </tr><tr>
                <td>Taux de capture</td><td><?php echo $pkm->get_taux(); ?></td>
            </tr>
        </table>    
    </div>


</div>

<div class="row"><h2>Statistiques de base</h2></div>

<div class="row">
    <table class="table table-striped table-bordered table-hover">
        <tr>
            <th class="name-stat">PV</th><td class="val-stat"><?php echo $pkm->get_hp(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_hp())) ?>%"></div>
            </td>
        </tr><tr>
            <th>Attaque</th><td><?php echo $pkm->get_atk(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_atk())) ?>%"></div>
            </td>
        </tr><tr>
            <th>Défence</th><td><?php echo $pkm->get_def(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_def())) ?>%"></div>
            </td>
        </tr><tr>
            <th>Atq. Spéciale</th><td><?php echo $pkm->get_spatk(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_spatk())) ?>%"></div>
            </td>
        </tr><tr>
            <th>Déf. Spéciale</th><td><?php echo $pkm->get_spdef(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_spdef())) ?>%"></div>
            </td>
        </tr><tr>
            <th>Vitesse</th><td><?php echo $pkm->get_speed(); ?></td>
            <td class="progress progess-stat">
                <div class="progress-bar progress-bar-info" style="width: <?php echo round($pkm->progress_ratio($pkm->get_speed())) ?>%"></div>
            </td>
        </tr>


    </table>
</div>

<div class="row"><h2>Evolution</h2></div>
<div class="row">
    <div class="evo-chain"><?php echo Pokemon_Ui::make_evo_chain($pkm); ?></div>
</div>

<div class="row"><h2>Faiblesses/Résistances</h2></div>
<div class="row text-center">
    <div><?php echo Type_Ui::make_res($pkm->get_type1(), $pkm->get_type2()); ?></div>
</div>
    
<div class="row"><h2>Attaques</h2></div>