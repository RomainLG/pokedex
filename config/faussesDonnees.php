<?php
$fauxMessages = array(
    new Message(array(
      "id" => 1,
      "contenu" => "test contenu 1",
      "sujet" => "test sujet 1",
      "lu" => false,
      "date" => "013-05-13 13:45"
    )),
    new Message(array(
      "id" => 2,
      "contenu" => "test contenu 2",
      "sujet" => "test sujet 2",
      "lu" => false,
      "date" => "2013-05-13 15:16"
    )),
    new Message(array(
      "id" => 3,
      "contenu" => "test contenu 3",
      "sujet" => "test sujet 3",
      "lu" => false,
      "date" => "2013-05-13 19:35"
    ))
);

$fauxDestinataires = array(
  array(
    "id" => 6,
    "nom" => "toto"
  ),
  array(
    "id" => 8,
    "nom" => "tata"
  ),
  array(
    "id" => 9,
    "nom" => "titi"
  )
);
?>