<?php
header("Content-Type: text/html; charset=utf8");
setlocale(LC_ALL, 'fr_FR.utf8');
date_default_timezone_set('Europe/Paris');
mb_internal_encoding("UTF-8");

require_once("./config/config.php");

$titre = "Pokédex";
$c = "";
$squelette = BASE_FILE . "ui/pages/nav-fixed.html.php";

$a = isset($_GET['a']) ? $_GET['a'] : 'home';

switch ($a) {
    case "home":
        ob_start();
        require_once("ui/pages/home.html.php");
        $c = ob_get_contents();
        ob_end_clean();
        break;
    
    case "typechart":
        $titre = "Table des types";
        ob_start();
        require_once("ui/pages/typechart.html.php");
        $c = ob_get_contents();
        ob_end_clean();
        break;

    case "search":
        if (isset($_GET['s'])) {
            //search by name
            $liste = Pokemon_Db::search($_GET['s']);
            if (count($liste) == 0) {
                //no results
                ob_start();
                require_once("ui/pages/noresults.html.php");
                $c = ob_get_contents();
                ob_end_clean();
                break;
            } elseif (count($liste) == 1) {
                //only 1 result : show it
                $pkm = Pokemon_Db::get_by_name($liste[0]);
            } else {
                //multiple results
                ob_start();
                require_once("ui/pages/list.html.php");
                $c = ob_get_contents();
                ob_end_clean();
                break;
            }
        }

    case "fiche":
        if (!isset($pkm)) {
            $pkm = Pokemon_Db::get_by_name(0);
            if (isset($_GET['na'])) {
                $pkm = Pokemon_Db::get_by_name($_GET['na']);
            }
            if (isset($_GET['id'])) {
                $pkm = Pokemon_Db::get_by_id($_GET['id']);
            }
        }
        $titre = "Pokedex : " . $pkm->get_nom();
        if ($pkm->get_id() != 0) {
            ob_start();
            require_once("ui/pages/fiche.html.php");
            $c = ob_get_contents();
            ob_end_clean();
            break;
        }
    default:
        ob_start();
        require_once("ui/pages/404.html.php");
        $c = ob_get_contents();
        ob_end_clean();
        break;
}



ob_start();
require_once($squelette);
$html = ob_get_contents();
ob_end_clean();

echo $html;
?>