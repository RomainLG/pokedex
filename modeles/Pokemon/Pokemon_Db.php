<?php

class Pokemon_Db {

    public static function get_by_id($id) {
        $id = (int) $id;
        
        $pkm = new Pokemon($id);
        self::set_stats($pkm);
        self::set_types($pkm);
        self::set_names($pkm);
        return $pkm;
    }

    public static function get_by_name($name) {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $stmt = $base->prepare("SELECT ps.id
			FROM pokemon_species AS ps 
			JOIN pokemon_species_names AS psn ON ps.id = psn.pokemon_species_id
			WHERE name = :name 
			AND psn.local_language_id = 5");
        $stmt->bindValue(':name', $name, SQLITE3_TEXT);
        $results = $stmt->execute();

        //$results = $base->query($req);
        $row = $results->fetchArray();
        $pkm = new Pokemon($row['id']);
        self::set_stats($pkm);
        self::set_types($pkm);
        self::set_names($pkm);
        return $pkm;
    }

    public static function search($req) {
        $names = array();
        $base = new SQLite3("veekun-pokedex.sqlite");
        
        $ucreq = mb_strtoupper( mb_substr( $req, 0, 1 )) . mb_substr( $req, 1 );

        $results = $base->query("SELECT ps.id, psn.name
	FROM pokemon_species AS ps 
	JOIN pokemon_species_names AS psn ON ps.id = psn.pokemon_species_id
	WHERE (name LIKE '%" . $req . "%' OR name LIKE '%" . $ucreq . "%')
	AND psn.local_language_id = 5");

        while ($row = $results->fetchArray()) {
            $names[] = $row['name'];
        }
        return $names;
    }
    

    public static function set_stats($pkm) {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $stmt = $base->prepare("SELECT ps.id, ps.capture_rate, ps.evolution_chain_id, pst.*, p.height, p.weight, psn.genus
			FROM pokemon_species AS ps 
			JOIN pokemon_stats AS pst ON ps.id = pst.pokemon_id
                        JOIN pokemon AS p ON ps.id = p.id
                        JOIN pokemon_species_names AS psn ON psn.pokemon_species_id = p.species_id
			WHERE ps.id = :id
                        AND psn.local_language_id = 5");

        $stmt->bindValue(':id', $pkm->get_id(), SQLITE3_INTEGER);
        $results = $stmt->execute();

        while ($row = $results->fetchArray()) {
            if ($row["stat_id"] == 1) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " PV");
                }
                $pkm->set_hp($row["base_stat"]);
                $pkm->set_taux($row["capture_rate"]);
                $pkm->set_taille(round($row["height"] / 10, 1));
                $pkm->set_poids(round($row["weight"] / 10, 1));
                $pkm->set_espece($row['genus']);
                $pkm->set_evo($row['evolution_chain_id']);
            } elseif ($row["stat_id"] == 2) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " Attaque");
                }
                $pkm->set_atk($row["base_stat"]);
            } elseif ($row["stat_id"] == 3) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " Défense");
                }
                $pkm->set_def($row["base_stat"]);
            } elseif ($row["stat_id"] == 4) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " Atk Spé.");
                }
                $pkm->set_spatk($row["base_stat"]);
            } elseif ($row["stat_id"] == 5) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " Dèf Spé.");
                }
                $pkm->set_spdef($row["base_stat"]);
            } elseif ($row["stat_id"] == 6) {
                if ($row['effort'] > 0) {
                    $pkm->set_ev($row['effort'] . " Vitesse");
                }
                $pkm->set_speed($row["base_stat"]);
            } elseif ($row) {
                
            }
        }
    }

    public static function set_names($pkm) {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $stmt = $base->prepare("SELECT p.id, psn.name, psn.local_language_id
                        FROM pokemon AS p
                        JOIN pokemon_species_names AS psn ON p.species_id = psn.pokemon_species_id
			WHERE p.id = :id ");

        $stmt->bindValue(':id', $pkm->get_id(), SQLITE3_INTEGER);
        $res = $stmt->execute();

        while ($row = $res->fetchArray()) {
            if ($row["local_language_id"] == 1) {
                $pkm->set_nom_jp($row["name"]);
            } elseif ($row["local_language_id"] == 9) {
                $pkm->set_nom_us($row["name"]);
            } elseif ($row["local_language_id"] == 5) {
                $pkm->set_nom($row["name"]);
            }
        }
    }

    public static function set_types($pkm) {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $stmt = $base->prepare("SELECT ps.id, pt.type_id, pt.slot, tn.name
			FROM pokemon_species AS ps 
			JOIN pokemon_types AS pt ON ps.id = pt.pokemon_id 
			JOIN type_names AS tn ON pt.type_id = tn.type_id
			WHERE id = :id AND tn.local_language_id = 5");

        $stmt->bindValue(':id', $pkm->get_id(), SQLITE3_INTEGER);
        $res = $stmt->execute();

        while ($row = $res->fetchArray()) {
            if ($row["slot"] == 1) {
                $pkm->set_type1($row["name"]);
            } elseif ($row["slot"] == 2) {
                $pkm->set_type2($row["name"]);
            }
        }
    }

}

?>