<?php


class Pokemon{
	private $id;
	private $nom;
	private $hp;
	private $atk;
	private $def;
	private $spatk;
	private $spdef;
	private $speed;
	private $type1;
	private $type2;
        
        private $nom_us;
        private $nom_jp;
        private $taille;
        private $poids;
        private $espece;
        private $ev;
        private $taux;
        
        private $evo;

        
        public function __construct($id){
		$this->id = $id;
	}
        
        public function set_nom($new){
		$this->nom = $new;
	}
        
	public function set_hp($new){
		$this->hp = $new;
	}

	public function set_atk($new){
		$this->atk = $new;
	}

	public function set_def($new){
		$this->def = $new;
	}

	public function set_spatk($new){
		$this->spatk = $new;
	}

	public function set_spdef($new){
		$this->spdef = $new;
	}

	public function set_speed($new){
		$this->speed = $new;
	}

	public function set_type1($new){
		$this->type1 = $new;
	}

	public function set_type2($new){
		$this->type2 = $new;
	}
        
        public function set_nom_us($nom_us) {
            $this->nom_us = $nom_us;
        }
        
        public function set_nom_jp($nom_jp) {
            $this->nom_jp = $nom_jp;
        }

        public function set_taille($taille) {
            $this->taille = $taille;
        }

        public function set_poids($poids) {
            $this->poids = $poids;
        }

        public function set_espece($espece) {
            $this->espece = $espece;
        }

        public function set_ev($ev) {
            $this->ev = $ev;
        }

        public function set_taux($taux) {
            $this->taux = $taux;
        }
        
        public function set_evo($evo) {
            $this->evo = $evo;
        }

	public function get_nom(){
		return $this->nom;
	}

	public function get_id(){
		return $this->id;
	}
        public function get_hp() {
            return $this->hp;
        }

        public function get_atk() {
            return $this->atk;
        }

        public function get_def() {
            return $this->def;
        }

        public function get_spatk() {
            return $this->spatk;
        }

        public function get_spdef() {
            return $this->spdef;
        }

        public function get_speed() {
            return $this->speed;
        }

        public function get_type1() {
            return $this->type1;
        }

        public function get_type2() {
            return $this->type2;
        }
        
        public function get_nom_us() {
            return $this->nom_us;
        }
        
        public function get_nom_jp() {
            return $this->nom_jp;
        }

        public function get_taille() {
            return $this->taille;
        }

        public function get_poids() {
            return $this->poids;
        }

        public function get_espece() {
            return $this->espece;
        }

        public function get_ev() {
            return $this->ev;
        }

        public function get_taux() {
            return $this->taux;
        }
        
        public function get_evo() {
            return $this->evo;
        }

        public function progress_ratio($val){
            return $val / 240 * 100;
        }
}


?>