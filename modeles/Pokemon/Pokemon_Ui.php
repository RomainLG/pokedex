<?php

class Pokemon_Ui {

    public static function make_evo_chain($pkm) {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $stmt = $base->prepare("SELECT ps.id pid, ps.identifier, ps.evolves_from_species_id, pe.*
            FROM pokemon_species AS ps
            LEFT JOIN pokemon_evolution AS pe ON ps.id = pe.evolved_species_id
            WHERE evolution_chain_id = :id
            ORDER BY id");

        $stmt->bindValue(':id', $pkm->get_evo(), SQLITE3_INTEGER);
        $results = $stmt->execute();
        
        $tab_evo = self::dbEvoToTab($results);
        //var_dump($tab_evo);
        $html = "<section><p>&nbsp;</p>";
        $last_stade = 0;
        foreach ($tab_evo as $id => $evo) {
            $p = Pokemon_Db::get_by_id($id);
            if ($last_stade != $evo['stade']){
                $last_stade = $evo['stade'];
                $html .= "</section><section>"; //chaque section est un niveau d'évolution
            }
            $html .= "<div>";
            if ($evo['stade'] != 0){
                $triggers =  "";
                foreach ($evo['trigger'] as $trigger) {
                    $triggers .= $trigger . " ; ";
                }
                $html .= "<p>" . $triggers . "</p>";                
            }
            $html .= '<a href="index.php?a=fiche&id=' . $id . '">';
            $html .= "<img src=".Pokemon_Ui::get_image($id)."></a>";
            $html .= "<p ";
            $html .= ($id == $pkm->get_id()) ? "class='current'" : "";
            $html .= ">" . $p->get_nom() . "</p></div>";
        }
        return $html . "</section>";
        //return self::evo_html($results);
    }
    
    public static function dbEvoToTab($results){
        $tab = array();    
        $stade = 0;
        $prev_id = 0;
        
        while ($row = $results->fetchArray()) {
            
            if ($row['evolves_from_species_id'] != $prev_id) {
                //New level of evolution
                $prev_id = $row['evolves_from_species_id'];
                $stade ++;
            }
            
            $tab[$row['pid']]['trigger'] = self::get_triggers($row);
            $tab[$row['pid']]['stade'] = $stade;
        }
        return $tab;
    }
    
    public static function get_triggers($row){    
        $ret = array();
        switch ($row['evolution_trigger_id']) {
            case 1:
                //evolve by level 
                if ($row['minimum_level'] != NULL) {
                    $ret[] = "Niveau " . $row['minimum_level'];
                }
                if ($row['time_of_day'] != NULL) {
                    $str = "Gagne un niveau ";
                    $str .= ($row['time_of_day'] == 'day') ? "de jour" : "de nuit";
                    $ret[] = $str;
                    
                }
                if ($row['minimum_happiness'] != NULL) {
                    $ret[] = $row['minimum_happiness'] . " de bonheur";
                }
                if ($row['minimum_beauty'] != NULL) {
                    $ret[] = $row['minimum_beauty'] . " de beauté";
                }
                if ($row['location_id'] != NULL) {
                    $ret[] = "Gagne un niveau prés de " . $row['location_id'];
                }
                if ($row['relative_physical_stats'] !== NULL) {                    
                    $ret[] = "relative_physical_stats =  " . $row['relative_physical_stats'];
                }
                break;

            case 2:
                //echange
                $str = "Echange";
                $str .= ($row['held_item_id'] != NULL) ? " en tenant " .
                        $row['held_item_id'] : "";
                $ret[] = $str;
                break;

            case 3:
                //evolve with stone (trigger_item_id)
                $ret[] = "Objet " . $row['trigger_item_id'];
                break;

            default:
                break;
        }
        return $ret;
    }
    
    public static function get_image($id) {
        return "pokemon/sugimori/".$id.".png";
    }

    public static function type_button($type) {
        switch ($type) {
            case "Plante":
                return "<a href='#' class='type-plante type-large'>Plante</a>";
            case "Poison":
                return "<a href='#' class='type-poison type-large'>Poison</a>";
            case "Feu":
                return "<a href='#' class='type-feu type-large'>Feu</a>";
            case "Vol":
                return "<a href='#' class='type-vol type-large'>Vol</a>";
            case "Eau":
                return "<a href='#' class='type-eau type-large'>Eau</a>";
            case "Normal":
                return "<a href='#' class='type-normal type-large'>Normal</a>";
            case "Insecte":
                return "<a href='#' class='type-insecte type-large'>Insecte</a>";
            case "Psy":
                return "<a href='#' class='type-psy type-large'>Psy</a>";
            case "Sol":
                return "<a href='#' class='type-sol type-large'>Sol</a>";
            case "Combat":
                return "<a href='#' class='type-combat type-large'>Combat</a>";
            case "Glace":
                return "<a href='#' class='type-glace type-large'>Glace</a>";
            case "Electrik":
                return "<a href='#' class='type-electrik type-large'>Electrik</a>";
            case "Roche":
                return "<a href='#' class='type-roche type-large'>Roche</a>";
            case "Spectre":
                return "<a href='#' class='type-spectre type-large'>Spectre</a>";
            case "Dragon":
                return "<a href='#' class='type-dragon type-large'>Dragon</a>";
            case "Ténèbres":
                return "<a href='#' class='type-tenebres type-large'>Ténèbres</a>";
            case "Acier":
                return "<a href='#' class='type-acier type-large'>Acier</a>";
            case "Fée":
                return "<a href='#' class='type-fee type-large'>Fée</a>";
            default:
                return "<a href='#' class='type-large'>" . $type . "</a>";
        }
    }
    
    public static function type_mini($type) {
        switch ($type) {
            case "Plante":
                return "<a href='#' class='type-plante type-mini'>Plante</a>";
            case "Poison":
                return "<a href='#' class='type-poison type-mini'>Poison</a>";
            case "Feu":
                return "<a href='#' class='type-feu type-mini'>Feu</a>";
            case "Vol":
                return "<a href='#' class='type-vol type-mini'>Vol</a>";
            case "Eau":
                return "<a href='#' class='type-eau type-mini'>Eau</a>";
            case "Normal":
                return "<a href='#' class='type-normal type-mini'>Normal</a>";
            case "Insecte":
                return "<a href='#' class='type-insecte type-mini'>Insect</a>";
            case "Psy":
                return "<a href='#' class='type-psy type-mini'>Psy</a>";
            case "Sol":
                return "<a href='#' class='type-sol type-mini'>Sol</a>";
            case "Combat":
                return "<a href='#' class='type-combat type-mini'>Combat</a>";
            case "Glace":
                return "<a href='#' class='type-glace type-mini'>Glace</a>";
            case "Electrik":
                return "<a href='#' class='type-electrik type-mini'>Electr</a>";
            case "Roche":
                return "<a href='#' class='type-roche type-mini'>Roche</a>";
            case "Spectre":
                return "<a href='#' class='type-spectre type-mini'>Spectre</a>";
            case "Dragon":
                return "<a href='#' class='type-dragon type-mini'>Dragon</a>";
            case "Ténèbres":
                return "<a href='#' class='type-tenebres type-mini'>Ténèbr</a>";
            case "Acier":
                return "<a href='#' class='type-acier type-mini'>Acier</a>";
            case "Fée":
                return "<a href='#' class='type-fee type-mini'>Fée</a>";
            default:
                return "<a href='#' class='type-mini'>" . $type . "</a>";
        }
    }

}
?>