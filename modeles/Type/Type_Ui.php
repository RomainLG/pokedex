<?php

class Type_Ui {
    
    public static function make_res($t1, $t2){
        $data = Type_Db::get_efficacy_name();
        $my = array(
            "Normal" => 1,
            "Combat" => 1,
            "Vol" => 1,
            "Poison" => 1,
            "Sol" => 1,
            "Roche" => 1,
            "Insecte" => 1,
            "Spectre" => 1,
            "Acier" => 1,
            "Feu" => 1,
            "Eau" => 1,
            "Plante" => 1,
            "Electrik" => 1,
            "Psy" => 1,
            "Glace" => 1,
            "Dragon" => 1,
            "Ténèbres" => 1
        );
        while ($row = $data->fetchArray()) {
            if ($row['target_type'] == $t1 || $row['target_type'] == $t2) {
                $my[$row['atk_type']] *= $row['damage_factor'] / 100.;
            }
        }
                
        $html = "<div class='table-responsive'>
            <table class='type-table text-center table table-bordered table-hover'>
            <tr>
            <td>" . Pokemon_Ui::type_mini("Normal") . "</td>
            <td>" . Pokemon_Ui::type_mini("Combat") . "</td>
            <td>" . Pokemon_Ui::type_mini("Vol") . "</td>
            <td>" . Pokemon_Ui::type_mini("Poison") . "</td>
            <td>" . Pokemon_Ui::type_mini("Sol") . "</td>
            <td>" . Pokemon_Ui::type_mini("Roche") . "</td>
            <td>" . Pokemon_Ui::type_mini("Insecte") . "</td>
            <td>" . Pokemon_Ui::type_mini("Spectre") . "</td>
            <td>" . Pokemon_Ui::type_mini("Acier") . "</td>
            <td>" . Pokemon_Ui::type_mini("Feu") . "</td>
            <td>" . Pokemon_Ui::type_mini("Eau") . "</td>
            <td>" . Pokemon_Ui::type_mini("Plante") . "</td>
            <td>" . Pokemon_Ui::type_mini("Electrik") . "</td>
            <td>" . Pokemon_Ui::type_mini("Psy") . "</td>
            <td>" . Pokemon_Ui::type_mini("Glace") . "</td>
            <td>" . Pokemon_Ui::type_mini("Dragon") . "</td>
            <td>" . Pokemon_Ui::type_mini("Ténèbres") . "</td></tr><tr>";              
        
        foreach ($my as $type => $value) {
            switch ($value) {
                case 1:
                    $html .= "<td class='dmg1'>1</td>";
                    break;
                case 0.25:
                    $html .= "<td class='dmg025'>1/4</td>";
                    break;                    
                case 0.5:
                    $html .= "<td class='dmg05'>1/2</td>";
                    break;
                case 2:
                    $html .= "<td class='dmg2'>2</td>";
                    break;
                case 4:
                    $html .= "<td class='dmg4'>4</td>";
                    break;
                case 0:
                    $html .= "<td class='dmg0'>0</td>";
                    break;
                default:
                    $html .= "<td class='warning'>oops</td>";
                    break;
            }
        }
        //var_dump($my);        
        return $html . "</tr></table></div>";
    }

       
    public static function make_table(){
        $data = Type_Db::get_efficacy();
        $html = "<div class='table-responsive'>
            <table class='type-table text-center table table-bordered table-hover'>
            <tr><td>
            Atk <span class='glyphicon glyphicon-arrow-down'></span>
            Déf <span class='glyphicon glyphicon-arrow-right'></span>
            </td>
            <td>" . Pokemon_Ui::type_mini("Normal") . "</td>
            <td>" . Pokemon_Ui::type_mini("Combat") . "</td>
            <td>" . Pokemon_Ui::type_mini("Vol") . "</td>
            <td>" . Pokemon_Ui::type_mini("Poison") . "</td>
            <td>" . Pokemon_Ui::type_mini("Sol") . "</td>
            <td>" . Pokemon_Ui::type_mini("Roche") . "</td>
            <td>" . Pokemon_Ui::type_mini("Insecte") . "</td>
            <td>" . Pokemon_Ui::type_mini("Spectre") . "</td>
            <td>" . Pokemon_Ui::type_mini("Acier") . "</td>
            <td>" . Pokemon_Ui::type_mini("Feu") . "</td>
            <td>" . Pokemon_Ui::type_mini("Eau") . "</td>
            <td>" . Pokemon_Ui::type_mini("Plante") . "</td>
            <td>" . Pokemon_Ui::type_mini("Electrik") . "</td>
            <td>" . Pokemon_Ui::type_mini("Psy") . "</td>
            <td>" . Pokemon_Ui::type_mini("Glace") . "</td>
            <td>" . Pokemon_Ui::type_mini("Dragon") . "</td>
            <td>" . Pokemon_Ui::type_mini("Ténèbres") . "</td></tr>";        
        $num_row = 0;
        
        while ($row = $data->fetchArray()) {
            if ($num_row < $row['damage_type_id']) {
                //new ligne
                if ($num_row == 0){
                    //first line : don't close line
                }else{
                    $html .= "</tr>";
                }
                $html .= "<tr><td>" . Pokemon_Ui::type_mini($row['name']) . "</td>";
                $num_row = $row['damage_type_id'];
            }
            switch ($row['damage_factor']) {
                case 100:
                    $html .= "<td class='dmg1'></td>";
                    break;
                case 50:
                    $html .= "<td class='dmg05'>1/2</td>";
                    break;
                case 200:
                    $html .= "<td class='dmg2'>2</td>";
                    break;
                case 0:
                    $html .= "<td class='dmg0'>0</td>";
                    break;
                default:
                    $html .= "<td class='warning'>oops</td>";
                    break;
            }
            
            //var_dump($row);
        }
        return $html . "</table></div>";
    }


}

?>
