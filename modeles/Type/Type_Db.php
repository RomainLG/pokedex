<?php

class Type_Db {
    public static function get_efficacy() {
        $base = new SQLite3("veekun-pokedex.sqlite");

        $data = $base->query("SELECT  tn.name, te.* 
            FROM type_efficacy AS te
            JOIN type_names AS tn ON tn.type_id = te.damage_type_id
            WHERE local_language_id = 5");
        
        return $data;
    }
    
    public static function get_efficacy_name(){
        $base = new SQLite3("veekun-pokedex.sqlite");

        $data = $base->query("SELECT tn2.name AS atk_type, tn1.name AS target_type, te.damage_factor
            FROM type_efficacy AS te
            JOIN type_names AS tn1 ON tn1.type_id = te.target_type_id
            JOIN type_names AS tn2 ON tn2.type_id = te.damage_type_id
            WHERE tn1.local_language_id = 5
            AND tn2.local_language_id = 5");
        
        return $data;
    }
}

?>
