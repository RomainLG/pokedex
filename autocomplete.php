<?php
header( "Content-Type: text/html; charset=utf8" );
setlocale (LC_ALL, 'fr_FR.utf8');
date_default_timezone_set('Europe/Paris');
mb_internal_encoding("UTF-8");

$param = $_GET['term'];
$options = array();

$ucparam = mb_strtoupper( mb_substr( $param, 0, 1 )) . mb_substr( $param, 1 );

$base=new SQLite3("veekun-pokedex.sqlite"); 

$results = $base->query("SELECT name
FROM pokemon_species_names
WHERE (name LIKE '%".$param."%'
OR name LIKE '%".$ucparam."%')
AND local_language_id = 5");

while ($row = $results->fetchArray()) {
    $options[] = $row['name'];
}
	
print json_encode($options);
?>